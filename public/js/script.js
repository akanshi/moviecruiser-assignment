let movieData = [];//to store movies
let favData = [];//to store favorite movies

function getMovies() {
	return fetch("http://localhost:3000/movies", { method: "GET" })
		.then((res) => {
			if (res.ok) {
				return res.json()
			}
			else {
				return Promise.reject(res.status)
			}
		})
		.then((x) => {
			movieData = x
			displayCollection(movieData)
			return x
		})
		
}
//get movies is calling display collection
let displayCollection = (x) => {
	let mainId = document.getElementById("moviesList");
	let htmlString = "";
	x.forEach(element => {
		htmlString += `<div class="needSpace card-deck bg-secondary w-50">
		              <img class = "card-img-top" src = '${element.posterPath}' width = 50%>
		              <li><i><b>${element.title}</b></i></li>		
					  <li><button class='btn btn-primary' onclick='addFavourite(${element.id})'>AddToFavourites</button></li>
					  </div>`

	})
	mainId.innerHTML = htmlString
}

function getFavourites() {
	return fetch("http://localhost:3000/favourites", { method: "GET" })
		.then((res) => {
			if (res.ok) {
				return res.json()
			}
			else {
				return Promise.reject(res.status)
			}
		})
		.then((x) => {
			favData = x
			displayFav(favData);
			return x
		})
		
}

function addFavourite(id) {
	let movie = movieData.find(x => {
		if (x.id == id) {
			return x
		}
	})

	let fav = favData.find(y => {
		if (y.id == movie.id) {
			return y
		}
	})

	if(fav) {
		alert("Movie is already added in favorites.");
        return Promise.reject(new Error('Movie is already added to favourites'));
    }else{
		return fetch(`http://localhost:3000/favourites`,{
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(movie)
		}
		).then(res => {
				if(res.ok){
					return res.json();
				}
			}
		).then(x => {
				favData.push(x);
				displayFav(favData);
				return favData;
			}
		)
	}
}

function deleteFavourite(id){


	return fetch(`http://localhost:3000/favourites/${id}`,{ method: 'DELETE'})
	.then(res => {
		if(res.ok){
			return res.json();
		}
	})
	.then(x => {
		favData.splice(id);
		displayFav(favData);
		return favData;
	}
)

}
//get favourites is calling displayfav
let displayFav = (x) => {
	let mainId = document.getElementById("favouritesList");
	let htmlString = "";
	x.forEach(element => {
		htmlString +=`<div class = "needSpace card-deck bg-info w-50">
		              <img class = "card-img-top" src = '${element.posterPath}' width = 50%>
		              <li><i><b>${element.title}</b></i></li>		
					  <li><button class='btn btn-danger' onclick='deleteFavourite(${element.id})'>Delete From Favourites</button></li>
					  </div>
					  `
	});
	mainId.innerHTML = htmlString

}




